
export const getQuestions = async () => {
        const questions = [];

        await fetch('https://opentdb.com/api.php?amount=20&category=22&difficulty=medium&type=multiple')
                .then(res => res.json())
                .then(data => data.results.forEach(element => {
                    questions.push(element);
                }));

        return questions;
    };