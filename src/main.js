import Vue from 'vue'
import App from './App.vue'
import StartPage from '../src/components/pages/StartPage.vue';
import QuestionPage from '../src/components/pages/QuestionPage.vue';
import ScorePage from '../src/components/pages/ScorePage.vue';
import VueRouter from 'vue-router';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: '*',
      redirect: '/start'
    },
    {
      path: '/start',
      name: 'StartPage',
      component: StartPage
    },
    {
      path: '/question',
      name: 'QuestionPage',
      component: QuestionPage
    },
    {
      path: '/score',
      name: 'ScorePage',
      component: ScorePage
    }
  ]
});

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
